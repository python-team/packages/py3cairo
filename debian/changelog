py3cairo (1.10.0+dfsg-6) UNRELEASED; urgency=medium

  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <novy@ondrej.org>  Tue, 29 Mar 2016 22:29:58 +0200

py3cairo (1.10.0+dfsg-5) unstable; urgency=medium

  * Team upload.
  * Add patches 80_fix-pickle.patch and 81_pickling-again.patch from Ubuntu
    to fix FTBFS (Closes: #800239)

 -- Scott Kitterman <scott@kitterman.com>  Sun, 27 Sep 2015 18:41:16 -0400

py3cairo (1.10.0+dfsg-4) unstable; urgency=medium

  * debian/patches/70_dont-link-libpython.patch
   - remove dependency to libpython (Closes: #734358, #739607)
  * debian/control
   - bump Standards-Version to 3.9.5
   - remove python-sphinx from build-deps and keep python3-sphinx

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Thu, 20 Feb 2014 22:10:33 +0900

py3cairo (1.10.0+dfsg-3) unstable; urgency=low

  * Upload to unstable (Closes: #707145)

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Wed, 15 May 2013 09:58:53 +0900

py3cairo (1.10.0+dfsg-3~exp4) experimental; urgency=low

  * rebuild for Python 3.3 (Closes: #703676)
  * debian/patches/60_python-config-without-interpreter.patch
   - fix waflib to support python 3.3
  * debian/control
   - update Standards-Version to 3.9.4
   - remove obsolete DMUA

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Sat, 30 Mar 2013 17:58:39 +0900

py3cairo (1.10.0+dfsg-3~exp3) experimental; urgency=low

  * debian/rules
   - fix build for other python3 versions (Closes: #691241)
  * debian/patches/50_specify-encoding-in-waf.patch
   - add to fix encoding probrem on python 3.3

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Sun, 28 Oct 2012 20:00:11 +0900

py3cairo (1.10.0+dfsg-3~exp2) experimental; urgency=low

  * debian/patches/101_pycairo-region.patch
   - add support for cairo_region_t (Closes: 688079)

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Thu, 20 Sep 2012 16:14:24 +0900

py3cairo (1.10.0+dfsg-3~exp1) experimental; urgency=low

  * debian/rules
   - support DEB_BUILD_OPTIONS=nocheck (Closes: #681034)
   - add -v flag to waf to output verbose log
  * debian/compat
   - up to 9 to add hardening flags
  * debian/control
   - update debhelper version to 9
   - enable DM-Upload

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Tue, 10 Jul 2012 22:52:04 +0900

py3cairo (1.10.0+dfsg-2) unstable; urgency=low

  * debian/rules
   - use pytest to run the test suite
  * debian/patches/10_test-target-py3.patch
   - add to change py.test script to use all versions of Python 3
  * debian/control
   - add build-depends: python3-pytest

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Mon, 16 Apr 2012 08:21:25 +0900

py3cairo (1.10.0+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #662957)

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Thu, 05 Apr 2012 23:34:52 +0900
